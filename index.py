from math import dist
import cv2
import numpy as np

# globals
np.set_printoptions(suppress = True, precision=8) # so that it doesn't do crazy scientific notation


def camera_to_head_rot(vive_plus_x, vive_plus_z):
  real_plus_x = -vive_plus_x
  real_plus_z = -vive_plus_z
  real_plus_y = np.cross(real_plus_z, real_plus_x)

  # Row major
  rot_head_to_camera = np.array([real_plus_x, real_plus_y, real_plus_z])

  rot_head_to_camera = np.array([[real_plus_x[0], real_plus_y[0], real_plus_z[0]], 
                                 [real_plus_x[1], real_plus_y[1], real_plus_z[1]], 
                                 [real_plus_x[2], real_plus_y[2], real_plus_z[2]], 
  ])
  rot_camera_to_head = np.linalg.inv(rot_head_to_camera) #*permute
  return rot_camera_to_head, rot_head_to_camera

cv2.namedWindow("hi", cv2.WINDOW_GUI_NORMAL + cv2.WINDOW_AUTOSIZE)                           


distLeft = np.array( [ 0.18863813209750457, 0.039099149779599926, -0.21062323375045208, 0.082877698258063251 ] )

fX = 415.27392253203089
fY = 415.55725761249829

cX = 484.57758532615168
cY = 481.68993133951881

cameraMatrixL = np.array([[fX, 0   , cX],
                  [0,    fY, cY],
                  [0,    0 ,  1]])


vive_plus_x_left = np.array([ -0.99614739609093017, 0.0082165877945687901, 0.087308449470288027 ])
vive_plus_z_left = np.array([ -0.087242319539971872, 0.0080899185618973843, -0.99615422666436526 ])

rot_left_to_head, rot_head_to_left = camera_to_head_rot(vive_plus_x_left, vive_plus_z_left)

distRight = np.array( [ 0.19732367961492914, 0.019489452687836879, -0.1938079474144194, 0.077990364704581136 ] )

cX = 512.8691428269675
cY = 492.42868562903908
fX = 416.76051703525695
fY = 417.16600544009145

cameraMatrixR = np.array([[fX, 0   , cX],
                  [0,    fY, cY],
                  [0,    0 ,  1]])

vive_plus_x_right = np.array([ -0.99731955465094546, -0.0047334751713241224, -0.073016325596983206 ])
vive_plus_z_right = np.array([ 0.07284842151579865, 0.029161476355195472, -0.99691664618593068 ])
rot_right_to_head, rot_head_to_right = camera_to_head_rot(vive_plus_x_right, vive_plus_z_right)

print(rot_left_to_head)
print(rot_right_to_head)

rot = np.array([[1, 0, 0],
                [0, 1, 0],
                [0, 0, 1]], dtype=np.float)

outMat = np.array([[480*.5, 0   , 480, 0],
                  [0,    480*.5, 480, 0],
                  [0,    0 ,  1, 0]], dtype=np.float)

def test(frameL, frameR, name):


  both = np.hstack((frameL, frameR))
  if (False):
    window_size = 2
    min_disp = 0
    # must be divisible by 16
    num_disp = 112*2 - min_disp
    max_disp = min_disp + num_disp
    stereo = cv2.StereoSGBM_create(minDisparity = min_disp,
                                    numDisparities = num_disp,
                                    blockSize = 16,
                                    P1 = 8*3*window_size**2,
                                    P2 = 32*3*window_size**2,
                                    disp12MaxDiff = 1,
                                    uniquenessRatio = 10,
                                    speckleWindowSize = 100,
                                    speckleRange = 32)


    disparity = stereo.compute(cv2.cvtColor(frameL, cv2.COLOR_BGR2GRAY), cv2.cvtColor(frameR, cv2.COLOR_BGR2GRAY)).astype(np.float32) / 16.0
    disparity = disparity[:,:]

    # convert disparity to 0-255 and color it
    disp_vis = 255*(disparity - min_disp)/ num_disp
    disp_color = cv2.applyColorMap(cv2.convertScaleAbs(disp_vis,1), cv2.COLORMAP_JET)

    # both = np.hstack((both, disp_color[:,max_disp:]))
    thing = np.hstack((disp_color, np.zeros((960,960,3), dtype=np.uint8)));
    both += (np.clip(thing, 0, 128));

  i = 0
  while i < 20:
    cv2.line(both,(0, i*48), (1920, i*48), (200,100,0))
    i+=1;
  
  i = 0
  while i < 40:
    cv2.line(both,(i*48, 0), (i*48, 960), (200,100,0))
    i+=1;



  BLACK = (255,255,255)
  font = cv2.FONT_HERSHEY_SIMPLEX
  font_size = 1.1
  font_color = BLACK
  font_thickness = 2
  x,y = 50, 50
  img_text = cv2.putText(both, name, (x,y), font, font_size, font_color, font_thickness, cv2.LINE_AA)

  cv2.imshow("hi", both)#np.hstack((both, disp_color[:,max_disp:])))
  # cv2.imshow("hi", disp_color)
  if cv2.waitKey(0) & 0xff == ord('q'):
    exit(0)

def bogo(frameL, frameR):
  test(frameL, frameR, "do-nothing")

def show_moses_way(frameL, frameR):
  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, rot_left_to_head,outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, rot_right_to_head ,outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(frameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(frameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, "moses original")

def show_way_2(frameL, frameR):
  left_to_right_trans_in_head = np.array([[-0.067489996552467346*2], [0], [0]])
  print(rot_head_to_left)
  print(rot_head_to_right)


  rot_left_camera_to_right_camera = np.matmul(rot_head_to_right, rot_left_to_head);

  print("Left to right camera is\n", rot_left_camera_to_right_camera);


  translate_left_to_right = rot_head_to_left.dot(np.array([[0.067489996552467346*2], [0], [0]]))
  print("wau2: ", translate_left_to_right)

  Rot1, Rot2, P1, P2, s, s, s = cv2.stereoRectify(cameraMatrixL, distLeft, cameraMatrixR, distRight, (960, 960), rot_left_camera_to_right_camera, translate_left_to_right, );




  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, Rot1 ,outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, Rot2  ,outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(frameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(frameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, "way2")

def show_way_3(frameL, frameR):
  # print(rot_head_to_left)
  # print(rot_head_to_right)


  rot_left_camera_to_right_camera = np.matmul(rot_head_to_right, rot_left_to_head);

  # print("Left to right camera is\n", rot_left_camera_to_right_camera);


  translate_left_to_right = rot_head_to_left.dot(np.array([[-0.067489996552467346*2], [0], [0]]))
  print("way3: ", translate_left_to_right)

  Rot1, Rot2, P1, P2, s, s, s = cv2.stereoRectify(cameraMatrixL, distLeft, cameraMatrixR, distRight, (960, 960), rot_left_camera_to_right_camera, translate_left_to_right, );

  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, Rot1 ,outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, Rot2  ,outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(frameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(frameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, "way3")

def show_way_4(frameL, frameR):

  print('WAY 4:')

  # print(rot_head_to_left)
  # print(rot_head_to_right)


  rot_left_camera_to_right_camera = np.matmul(rot_head_to_right, rot_left_to_head);

  print("Left to right camera is\n", rot_left_camera_to_right_camera);


  translate_left_to_right = np.array([[0.13446], [-0.01109], [-0.011785]])
  print(translate_left_to_right)

  Rot1, Rot2, P1, P2, s, s, s = cv2.stereoRectify(cameraMatrixL, distLeft, cameraMatrixR, distRight, (960, 960), rot_left_camera_to_right_camera, translate_left_to_right, );




  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, Rot1 ,outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, Rot2  ,outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(frameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(frameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, "way4")

# Totally wrong
def show_way_5(frameL, frameR):

  print('WAY 5:')

  # print(rot_head_to_left)
  # print(rot_head_to_right)


  rot_left_camera_to_right_camera = np.linalg.inv(np.matmul(rot_head_to_right, rot_left_to_head));

  print("Left to right camera is\n", rot_left_camera_to_right_camera);


  translate_left_to_right = np.array([[0.13446], [-0.01109], [-0.011785]])
  print(translate_left_to_right)

  Rot1, Rot2, P1, P2, s, s, s = cv2.stereoRectify(cameraMatrixL, distLeft, cameraMatrixR, distRight, (960, 960), rot_left_camera_to_right_camera, translate_left_to_right, );




  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, Rot1 ,outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, Rot2  ,outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(frameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(frameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, "way4")

def show_way_6(frameL, frameR):
  # print(rot_head_to_left)
  # print(rot_head_to_right)


  rot_left_camera_to_right_camera = np.matmul(rot_head_to_right, rot_left_to_head);

  # print("Left to right camera is\n", rot_left_camera_to_right_camera);


  translate_left_to_right = rot_left_to_head.dot(np.array([[-0.067489996552467346*2], [0], [0]]))
  print("way6e: ", translate_left_to_right)

  Rot1, Rot2, P1, P2, s, s, s = cv2.stereoRectify(cameraMatrixL, distLeft, cameraMatrixR, distRight, (960, 960), rot_left_camera_to_right_camera, translate_left_to_right, );

  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, Rot1 ,outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, Rot2  ,outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(frameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(frameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, "way6")

def show_way_7(inFrameL, inFrameR):
  # print(rot_head_to_left)
  # print(rot_head_to_right)


  rot_left_camera_to_right_camera = np.matmul(rot_head_to_right, rot_left_to_head);

  # print("Left to right camera is\n", rot_left_camera_to_right_camera);


  translate_left_to_right = rot_head_to_right.dot(np.array([[0.067489996552467346*2], [0], [0]]))
  print("way7: ", translate_left_to_right)

  Rot1, Rot2, P1, P2, s, s, s = cv2.stereoRectify(cameraMatrixL, distLeft, cameraMatrixR, distRight, (960, 960), rot_left_camera_to_right_camera, translate_left_to_right, );

  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, Rot1 ,outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, Rot2  ,outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(inFrameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(inFrameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, "way7")

  # return
  #This stuff wqas to check my answer. Not what you want.
  rot_from_stereo_to_hmd_1 = np.matmul(rot_left_to_head, np.linalg.inv(Rot1))
  print(rot_from_stereo_to_hmd_1)
  rot_from_stereo_to_hmd_2 = np.matmul(rot_right_to_head, np.linalg.inv(Rot2))
  # print(rot_from_stereo_to_hmd_2)
  #Both of these should be exactly the same, discounting floating point errors.

  check_answer_should_be_rot_left_to_head = np.matmul(rot_from_stereo_to_hmd_1, Rot1);
  print(rot_left_to_head)
  print(check_answer_should_be_rot_left_to_head)

  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, np.matmul(rot_from_stereo_to_hmd_1, Rot1) ,outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, np.matmul(rot_from_stereo_to_hmd_1, Rot2)  ,outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(inFrameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(inFrameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, "way7-checkanswer")





if __name__ == "__main__":
  frame = cv2.imread("hi.png")
  frameL = frame[:,:960]
  frameR = frame[:,960:]
  while True:
    bogo(frameL, frameR)
    show_moses_way(frameL, frameR);
    # show_way_2(frameL, frameR);
    # show_way_3(frameL, frameR);
    # show_way_4(frameL, frameR);
    # show_way_6(frameL, frameR)
    show_way_7(frameL, frameR)
