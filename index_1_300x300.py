from math import dist
import cv2
import numpy as np

# real_plus_x_left = -vive_plus_x_left
# real_plus_z_left = -vive_plus_z_left
# real_plus_y_left = np.cross(real_plus_z_left, real_plus_x_left)

# # print(real_plus_x_left)
# # print(real_plus_y_left)
# # print(real_plus_z_left)

# rot_head_to_left = np.array([real_plus_x_left, real_plus_y_left, real_plus_z_left])

# rot_left_to_head = np.linalg.inv(rot_head_to_left)

def camera_to_head_rot(vive_plus_x, vive_plus_z):
  real_plus_x = -vive_plus_x
  real_plus_z = -vive_plus_z
  real_plus_x = [-vive_plus_x[0], vive_plus_x[1], vive_plus_x[2]]
  real_plus_z = [vive_plus_z[0], vive_plus_z[1], -vive_plus_z[2]]
  real_plus_y = np.cross(real_plus_z, real_plus_x)

  # Row major
  rot_head_to_camera = np.array([real_plus_x, real_plus_y, real_plus_z])

  rot_head_to_camera = np.array([[real_plus_x[0], real_plus_y[0], real_plus_z[0]], 
                                 [real_plus_x[1], real_plus_y[1], real_plus_z[1]], 
                                 [real_plus_x[2], real_plus_y[2], real_plus_z[2]], 
  ])

  # permute = np.array([[-1, 0,0], [0, 1, 0], [0, 0, -1]])

  rot_camera_to_head = np.linalg.inv(rot_head_to_camera) #*permute
  return rot_camera_to_head


def camera_to_head_rot(vive_plus_x, vive_plus_z):
  real_plus_x = -vive_plus_x
  real_plus_z = -vive_plus_z
  real_plus_y = np.cross(real_plus_z, real_plus_x)

  rot_head_to_camera = np.array([[real_plus_x[0], real_plus_y[0], real_plus_z[0]], 
                                 [real_plus_x[1], real_plus_y[1], real_plus_z[1]], 
                                 [real_plus_x[2], real_plus_y[2], real_plus_z[2]], 
  ])


  rot_camera_to_head = np.linalg.inv(rot_head_to_camera)
  return rot_camera_to_head



distLeft = np.array( [ 0.18863813209750457, 0.039099149779599926, -0.21062323375045208, 0.082877698258063251 ] )

fX = 415.27392253203089
fY = 415.55725761249829

cX = 484.57758532615168
cY = 481.68993133951881

cameraMatrixL = np.array([[fX, 0   , cX],
									[0,    fY, cY],
									[0,    0 ,  1]])


vive_plus_x_left = np.array([ -0.99614739609093017, 0.0082165877945687901, 0.087308449470288027 ])
vive_plus_z_left = np.array([ -0.087242319539971872, 0.0080899185618973843, -0.99615422666436526 ])

rot_left_to_head = camera_to_head_rot(vive_plus_x_left, vive_plus_z_left)

distRight = np.array( [ 0.19732367961492914, 0.019489452687836879, -0.1938079474144194, 0.077990364704581136 ] )

cX = 512.8691428269675
cY = 492.42868562903908
fX = 416.76051703525695
fY = 417.16600544009145

cameraMatrixR = np.array([[fX, 0   , cX],
									[0,    fY, cY],
									[0,    0 ,  1]])

vive_plus_x_right = np.array([ -0.99731955465094546, -0.0047334751713241224, -0.073016325596983206 ])
vive_plus_z_right = np.array([ 0.07284842151579865, 0.029161476355195472, -0.99691664618593068 ])
rot_right_to_head = camera_to_head_rot(vive_plus_x_right, vive_plus_z_right)

print(rot_left_to_head)
print(rot_right_to_head)

cap = cv2.VideoCapture(0)

rot = np.array([[1, 0, 0],
								[0, 1, 0],
								[0, 0, 1]], dtype=np.float)

outMat = np.array([[480*.5*.5, 0   , 480*.5, 0],
									[0,    480*.5*.5, 480*.5, 0],
									[0,    0 ,  1, 0]], dtype=np.float)

# print(cameraMatrixL)
# print(distLeft)
# print(rot)
# print(outMat)
# print((960,960))

(lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, rot_left_to_head,outMat, (480,480), m1type = cv2.CV_32FC1)

# print(cameraMatrixR)
# print(distRight)
# print(rot)
# print(outMat)
# print((960,960))
(rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, rot_right_to_head ,outMat, (480,480), m1type = cv2.CV_32FC1)



window_size = 15
min_disp = 0
# must be divisible by 16
num_disp = 112 - min_disp
max_disp = min_disp + num_disp
stereo = cv2.StereoSGBM_create(minDisparity = min_disp,
                                numDisparities = num_disp,
                                blockSize = 16,
                                P1 = 8*3*window_size**2,
                                P2 = 32*3*window_size**2,
                                disp12MaxDiff = 1,
                                uniquenessRatio = 10,
                                speckleWindowSize = 100,
                                speckleRange = 32)

while (True):
  ret, frame = cap.read()
  frameL = frame[:,:480]
  frameR = frame[:,480:]
  frameL = cv2.remap(frameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(frameR, rx, ry, cv2.INTER_LINEAR)
  disparity = stereo.compute(frameL, frameR).astype(np.float32) / 16.0
  # disparity = disparity[:,max_disp:]

  # convert disparity to 0-255 and color it
  disp_vis = 255*(disparity - min_disp)/ num_disp
  disp_color = cv2.applyColorMap(cv2.convertScaleAbs(disp_vis,1), cv2.COLORMAP_JET)

  both = np.hstack((frameL, frameR))

  i = 0
  while i < 20:
    cv2.line(both,(0, i*48*.5), (1920, i*48), (255,255,255))
    i+=1;

  cv2.imshow("hi", np.hstack((both, disp_color)))
  # cv2.imshow("hi", disp_color)
  cv2.waitKey(1)