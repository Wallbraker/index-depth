from math import dist
import cv2
import numpy as np

# globals
np.set_printoptions(suppress = True, precision=8) # so that it doesn't do crazy scientific notation


def camera_to_head_rot(vive_plus_x, vive_plus_z):
  real_plus_x = -vive_plus_x
  real_plus_z = -vive_plus_z
  real_plus_y = np.cross(real_plus_z, real_plus_x)

  # Row major
  rot_head_to_camera = np.array([real_plus_x, real_plus_y, real_plus_z])

  rot_head_to_camera = np.array([[real_plus_x[0], real_plus_y[0], real_plus_z[0]], 
                                 [real_plus_x[1], real_plus_y[1], real_plus_z[1]], 
                                 [real_plus_x[2], real_plus_y[2], real_plus_z[2]], 
  ])
  rot_camera_to_head = np.linalg.inv(rot_head_to_camera) #*permute
  return rot_camera_to_head, rot_head_to_camera

cv2.namedWindow("hi", cv2.WINDOW_GUI_NORMAL + cv2.WINDOW_AUTOSIZE)                           


###
# Left Camera
#

distLeft = np.array( [ 0.19040572786094423, 0.036894866222433854, -0.21877375625571835, 0.088694829218667004 ] )

cX = 494.02343809640882
cY = 479.57446949281336
fX = 416.44746021306213
fY = 416.77683176882692

cameraMatrixL = np.array([[fX, 0 , cX],
                          [0,  fY, cY],
                          [0,  0 ,  1]])

vive_plus_x_left = np.array([ -0.99633722026014748, 0.02491023395003289, 0.081802486775607697 ])
vive_plus_z_left = np.array([ -0.081483851484570657, 0.013550156710663008, -0.99658255968937626 ])

rot_left_to_head, rot_head_to_left = camera_to_head_rot(vive_plus_x_left, vive_plus_z_left)


###
# Right Camera
#

distRight = np.array( [ 0.19584415405896344, 0.021783768917423556, -0.19761409603187091, 0.080017823997918625 ] )

cX = 480.01726367020285
cY = 487.51619051671247
fX = 416.96065787984736
fY = 417.13323152545593

cameraMatrixR = np.array([[fX, 0 , cX],
                          [0,  fY, cY],
                          [0,  0 ,  1]])

vive_plus_x_right = np.array([ -0.99716332733202173, -0.0082579972300266397, -0.074813821842792338 ])
vive_plus_z_right = np.array([ 0.074608150094742809, 0.022901721612180993, -0.99694991795930954 ])
rot_right_to_head, rot_head_to_right = camera_to_head_rot(vive_plus_x_right, vive_plus_z_right)


###
# Other stuff
#

print(rot_left_to_head)
print(rot_right_to_head)

rot = np.array([[1, 0, 0],
                [0, 1, 0],
                [0, 0, 1]], dtype=np.float)

outMat = np.array([[480*.5, 0   , 480, 0],
                  [0,    480*.5, 480, 0],
                  [0,    0 ,  1, 0]], dtype=np.float)

def test(frameL, frameR, name):


  both = np.hstack((frameL, frameR))
  if (False):
    window_size = 2
    min_disp = 0
    # must be divisible by 16
    num_disp = 112*2 - min_disp
    max_disp = min_disp + num_disp
    stereo = cv2.StereoSGBM_create(minDisparity = min_disp,
                                    numDisparities = num_disp,
                                    blockSize = 16,
                                    P1 = 8*3*window_size**2,
                                    P2 = 32*3*window_size**2,
                                    disp12MaxDiff = 1,
                                    uniquenessRatio = 10,
                                    speckleWindowSize = 100,
                                    speckleRange = 32)


    disparity = stereo.compute(cv2.cvtColor(frameL, cv2.COLOR_BGR2GRAY), cv2.cvtColor(frameR, cv2.COLOR_BGR2GRAY)).astype(np.float32) / 16.0
    disparity = disparity[:,:]

    # convert disparity to 0-255 and color it
    disp_vis = 255*(disparity - min_disp)/ num_disp
    disp_color = cv2.applyColorMap(cv2.convertScaleAbs(disp_vis,1), cv2.COLORMAP_JET)

    # both = np.hstack((both, disp_color[:,max_disp:]))
    thing = np.hstack((disp_color, np.zeros((960,960,3), dtype=np.uint8)));
    both += (np.clip(thing, 0, 128));

  i = 0
  while i < 20:
    cv2.line(both,(0, i*48), (1920, i*48), (200,100,0))
    i+=1;
  
  i = 0
  while i < 40:
    cv2.line(both,(i*48, 0), (i*48, 960), (200,100,0))
    i+=1;



  BLACK = (255,255,255)
  font = cv2.FONT_HERSHEY_SIMPLEX
  font_size = 1.1
  font_color = BLACK
  font_thickness = 2
  x,y = 50, 50
  img_text = cv2.putText(both, name, (x,y), font, font_size, font_color, font_thickness, cv2.LINE_AA)

  cv2.imshow("hi", both)#np.hstack((both, disp_color[:,max_disp:])))
  # cv2.imshow("hi", disp_color)
  if cv2.waitKey(0) & 0xff == ord('q'):
    exit(0)

def bogo(frameL, frameR):
  test(frameL, frameR, "do-nothing")

def show_moses_way(frameL, frameR):
  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL,distLeft, rot_left_to_head, outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR,distRight, rot_right_to_head, outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(frameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(frameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, "moses original")


def show_with_R_T(inFrameL, inFrameR, R, T, name):
  Rot1, Rot2, P1, P2, Q = cv2.fisheye.stereoRectify(cameraMatrixL, distLeft, cameraMatrixR, distRight, (960, 960), R, T, 0)

  (lx,ly) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixL, distLeft,  Rot1, outMat, (960,960), m1type = cv2.CV_32FC1)
  (rx,ry) = cv2.fisheye.initUndistortRectifyMap(cameraMatrixR, distRight, Rot2, outMat, (960,960), m1type = cv2.CV_32FC1)

  frameL = cv2.remap(inFrameL, lx, ly, cv2.INTER_LINEAR)
  frameR = cv2.remap(inFrameR, rx, ry, cv2.INTER_LINEAR)
  test(frameL, frameR, name)


def show_way_7(inFrameL, inFrameR):
  translate_left_to_right = rot_head_to_right.dot(np.array([[0.067489996552467346*2], [0], [0]]))
  rot_left_camera_to_right_camera = np.matmul(rot_head_to_right, rot_left_to_head);
  T = translate_left_to_right
  R = rot_left_camera_to_right_camera
  show_with_R_T(inFrameL, inFrameR, R, T, "Way7")


def show_jakob_way(inFrameL, inFrameR):
  T = np.array([-0.122712, -0.002844, 0.009945])
  # [ -0.004733, -0.0783498, -0.0165679, 0.996777 ]
  R = np.array([[0.987174, 0.033771, -0.156038],
                [-0.032287, 0.999406, 0.012032],
                [0.156351, -0.006839, 0.987678]])

  show_with_R_T(frameL, frameR, R, T, "Jakob")

def show_jakob_hacks_way(inFrameL, inFrameR):
  T = np.array([-0.122712, 0.002844, -0.009945])
  # [ 0.004733, -0.0783498, -0.0165679, 0.996777 ]
  R = np.array([[0.987174, 0.033771, -0.156038],
                [-0.032287, 0.999406, -0.012032],
                [0.156351, 0.006839, 0.987678]])

  show_with_R_T(frameL, frameR, R, T, "Jakob hacks")


def show_opencv_way(inFrameL, inFrameR):
  T = np.array([-0.135467264970759, 0.002606066041219677, -0.009203010842189505])
  R = np.array([[0.9876674507858756, 0.03818112549144956, -0.1518394162078355],
                [-0.04092300705784093, 0.9990501099127046, -0.01497282126911513],
                [0.1511235062836818, 0.02100189371507117, 0.9882917617327927]])

  show_with_R_T(frameL, frameR, R, T, "OpenCV")


if __name__ == "__main__":
  frame = cv2.imread("hi.png")
  frameL = frame[:,:960]
  frameR = frame[:,960:]
  while True:
    bogo(frameL, frameR)
    show_jakob_way(frameL, frameR)
    show_way_7(frameL, frameR)
    show_jakob_hacks_way(frameL, frameR)
    show_opencv_way(frameL, frameR)
